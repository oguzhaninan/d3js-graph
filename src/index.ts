import d3, { Selection } from 'd3';

import { 
    nodes as nodeList,
    links as linkList
} from '../data';

type NodeList = d3.layout.force.Node[];
type LinkList = d3.layout.force.Link<any>[];

type Settings = {
    hideAllLabels: Boolean,
    pinAllNodes: Boolean
}

type Filter = {
    protocol: Set<String>,
}

class ForceDirectedGraph {
    
    private w: number = window.innerWidth;
    private h: number = window.innerHeight;

    private nodeSummaryBox: HTMLDivElement;
    private elementProtocolFilter: HTMLDivElement;
    private elementToggleLeftSideBtn: HTMLButtonElement;
    private elementZoomButtons: NodeListOf<HTMLButtonElement>;
    private elementCheckHideLabels: HTMLInputElement;
    private elementCheckPinAllNodes: HTMLInputElement;
    private elementCheckPinNode: HTMLInputElement;
    private elementBtnCenterNode: HTMLButtonElement;
    private elementBtnCenterView: HTMLButtonElement;
    private elementRightSidePanel: HTMLDivElement;
    private elementRightSideLinkPanel: HTMLDivElement;
    private elementLeftSidePanel: HTMLDivElement;
    private elementTotalNodeCount: HTMLLabelElement;
    private elementTotalLinkCount: HTMLLabelElement;
    private elementNodeRelationCount: HTMLLabelElement;

    private selectedNode = null;
    private selectedLink = null;
    private focusNode = null;
    private highlightNode = null;
    private textCenter = false;
    private highlightTrans = 0.1;
    private highlightColor = "#3498db";
    private nodeFocusColor = "#7f8c8d";
    private defaultNodeColor = "#95a5a6";
    private defaultLinkColor = "#888";
    private nominalBaseNodeSize = 12;
    private maxBaseNodeSize = 36;
    private nominalTextSize = 8;
    private maxTextSize = 24;
    private nominalStroke = 2;
    private maxStroke = 4.5;
    private minZoom = 0.1;
    private maxZoom = 10;
    private nodeShape = "circle";

    private filter: Filter;
    private settings: Settings;
    private nodeInfoFields: Array<Object>;
    private linkInfoFields: Array<Object>;

    private linkedByIndex: Object = {};

    private svg: Selection<any>;
    private zoom: d3.behavior.Zoom<any>;

    private nodes: NodeList;
    private links: LinkList;
    private force: d3.layout.Force<d3.layout.force.Link<d3.layout.force.Node>, d3.layout.force.Node>;

    private link: any;
    private node: any;
    private circle: any;
    private rect: any;
    private text: any;
    private g: any;

    constructor(nodes: NodeList, links: LinkList) {
        this.nodes = nodes;
        this.links = links;

        this.filter = {
            protocol: new Set(['tcp', 'udp']),
        };
        
        this.settings = {
            hideAllLabels: false,
            pinAllNodes: false,
        };

        this.nodeInfoFields = [
            { 'key': 'ip', 'label': 'IP' }, { 'key': 'zone', 'label': 'Zone' }, 
            { 'key': 'vendor', 'label': 'Vendor' }, { 'key': 'os', 'label': 'OS' }, { 'key': 'name', 'label': 'Name' }, 
            { 'key': 'firstSeen', 'label': 'First Seen' }, { 'key': 'lastSeen', 'label': 'Last Seen' }
        ];

        this.linkInfoFields = [
            { 'key': 'protocol', 'label': 'Protocol' }, { 'key': 'sourcePort', 'label': 'Source Port' }, 
            { 'key': 'destPort', 'label': 'Dest Port' }, { 'key': 'totalByte', 'label': 'Total Byte' }
        ];

        this.initElements();
        
        this.initElementEvents();

        this.svg = d3.select("#container").append("svg");
        this.zoom = d3.behavior.zoom().scaleExtent([this.minZoom, this.maxZoom]);

        this.force = d3.layout
            .force()
            .linkDistance(100)
            .charge(-300)
            .size([this.w, this.h]);

        d3.select(window).on("mouseup", () => this.onMouseUpWindow());

        d3.select(window).on("resize", () => this.resize);
        this.resize();

        this.zoom.on("zoom", () => this.onZoom());

        this.svg.call(this.zoom);
    }
    
    initElements(): void {
        this.nodeSummaryBox = <HTMLDivElement>document.getElementById('node-summary');
        this.elementProtocolFilter = <HTMLDivElement>document.getElementById('filter-protocol');
        this.elementToggleLeftSideBtn = <HTMLButtonElement>document.getElementById('toggle-left-side-panel');
        this.elementZoomButtons = <NodeListOf<HTMLButtonElement>>document.querySelectorAll('.btn-zoom');
        this.elementCheckHideLabels = <HTMLInputElement>document.getElementById('check-hide-labels');
        this.elementCheckPinAllNodes = <HTMLInputElement>document.getElementById('check-pin-all-nodes');
        this.elementCheckPinNode = <HTMLInputElement>document.getElementById('check-pin-node');
        this.elementBtnCenterNode = <HTMLInputElement>document.getElementById('btn-center-node');
        this.elementBtnCenterView = <HTMLButtonElement>document.getElementById('btn-center-view');
        this.elementRightSidePanel = <HTMLDivElement>document.getElementById('right-side-panel');
        this.elementRightSideLinkPanel = <HTMLDivElement>document.getElementById('right-side-link-panel');
        this.elementLeftSidePanel = <HTMLDivElement>document.getElementById('left-side-panel');
        this.elementTotalNodeCount = <HTMLLabelElement>document.getElementById('total-node-count');
        this.elementTotalLinkCount = <HTMLLabelElement>document.getElementById('total-link-count');
        this.elementNodeRelationCount = <HTMLLabelElement>document.getElementById('node-relation-count');
    }

    initElementEvents(): void {
        this.elementProtocolFilter.addEventListener('change', (e: Event) => this.handleChangeProtocolFilter(e));
        this.elementToggleLeftSideBtn.addEventListener('click', (e: Event) => this.handleClickToggleLeftSideBtn(e));
        this.elementCheckHideLabels.addEventListener('change', (e: Event) => this.handleChangeHideLabels(e));
        this.elementCheckPinAllNodes.addEventListener('change', (e: Event) => this.handleChangePinAllNodes(e));
        this.elementCheckPinNode.addEventListener('change', (e: Event) => this.handleCheckPinNode(e));
        this.elementBtnCenterNode.addEventListener('click', (e: Event) => this.handleClickCenterNode(e));
        this.elementBtnCenterView.addEventListener('click', (e: Event) => this.handleClickCenterViewBtn(e));

        this.elementZoomButtons.forEach((element: any) => {
            element.addEventListener('click', (e: Event) => this.handleClickZoomButton(e));
        });
    }

    onZoom(): void {
        var stroke = this.nominalStroke;
        if (this.nominalStroke * this.zoom.scale() > this.maxStroke) {
            stroke = this.maxStroke / this.zoom.scale();
        }

        this.link.style("stroke-width", stroke);
        this.circle.style("stroke-width", stroke);

        var baseRadius = this.nominalBaseNodeSize;
        if (this.nominalBaseNodeSize * this.zoom.scale() > this.maxBaseNodeSize) {
            baseRadius = this.maxBaseNodeSize / this.zoom.scale();
        }

        this.circle.attr("d", d3.svg.symbol()
            .size(Math.PI * Math.pow(baseRadius, 2))
            .type((d: any) => d.type));

        if (!this.textCenter) {
            this.text.attr("dx", baseRadius);
        }

        var textSize = this.nominalTextSize;
        if (this.nominalTextSize * this.zoom.scale() > this.maxTextSize) {
            textSize = this.maxTextSize / this.zoom.scale();
        }
        this.text.style("font-size", `${textSize}px`);

        this.g.attr("transform", `translate(${d3.event.translate}) scale(${d3.event.scale})`);
    }

    onMouseUpWindow(): any {
        if (this.focusNode !== null) {
            this.focusNode = null;
            if (this.highlightTrans < 1) {
                this.circle.style("opacity", 1);
                this.text.style("opacity", 1);
                this.link.style("opacity", 1);
            }
        }
        if (this.highlightNode === null) {
            this.exitHighlight();
        }
    }

    handleClickCenterViewBtn(_event: any): void {
        if (this.nodes.length === 0) return;

        // düğümlerin yayıldığı alan
        var minX = d3.min(this.nodes.map((d: any) => d.x));
        var minY = d3.min(this.nodes.map((d: any) => d.y));

        var maxX = d3.max(this.nodes.map((d: any) => d.x));
        var maxY = d3.max(this.nodes.map((d: any) => d.y));

        // graph boyutu
        var gWidth = maxX - minX;
        var gHeight = maxY - minY;

        // svg boyutunu graph boyutuna oranı
        var widthRatio = this.w / gWidth;
        var heightRatio = this.h / gHeight;

        var minRatio = Math.min(widthRatio, heightRatio) * 0.5;

        // yeni graph boyutu
        var newGraphWidth = gWidth * minRatio;
        var newGraphHeight = gHeight * minRatio;

        // ekranın orta kordinatları
        var xTrans = -minX * minRatio + (this.w - newGraphWidth) / 2;
        var yTrans = -minY * minRatio + (this.h - newGraphHeight) / 2;

        this.g.attr("transform", `translate(${xTrans},${yTrans}) scale(${minRatio})`);

        this.zoom.translate([xTrans, yTrans]);
        this.zoom.scale(minRatio);
    }

    /**
     * Sol taraftaki menüde filtere değiştiğinde 
     */
    changedFilter(): void {
        if (this.nodes.length > 1) {
            const _links = this.links.filter((l: any) => {
                const hasProtocol = this.filter.protocol.has(l.data.protocol);
                return hasProtocol;
            });

            this.link.style('display', (l: any) => {
                const hasProtocol = this.filter.protocol.has(l.data.protocol);
                return hasProtocol ? 'block': 'none';
            });

            this.node.style('display', (n: any) => {
                const hasConnection = _links.some(l => l.source.id == n.id) || _links.some(l => l.target.id == n.id);
                return hasConnection ? 'block' : 'none';
            });

            this.text.style('display', (t: any) => {
                const hasConnection = _links.some(l => l.source.id == t.id) || _links.some(l => l.target.id == t.id);
                return hasConnection ? 'block' : 'none';
            });
        }
    }

    handleChangeProtocolFilter(event: any): void {
        if (event.target.checked) {
            this.filter.protocol.add(event.target.value);
        } else {
            this.filter.protocol.delete(event.target.value);
        }
        this.changedFilter();
    }

    handleClickToggleLeftSideBtn(_event: any): void {
        let isExpand = this.elementToggleLeftSideBtn.value == "expand";
        this.elementLeftSidePanel.style.height = isExpand ? "250px" : "28px";
        this.elementToggleLeftSideBtn.value = isExpand ? "collapse" : "expand";
        this.elementToggleLeftSideBtn.textContent = isExpand ? "▲" : "▼";
    }

    handleClickZoomButton(event: any): void {
        const dataZoom = parseInt(event.target.attributes['data-zoom'].value);
        this.zoom.scale(this.zoom.scale() * Math.pow(1.2, dataZoom));

        const dcx = (window.innerWidth / 2 - (this.w / 2) * this.zoom.scale());
        const dcy = (window.innerHeight / 2 - (this.h / 2) * this.zoom.scale());
        this.zoom.translate([dcx, dcy]);

        this.svg.transition().duration(250).call(this.zoom.event);
    }

    handleChangePinAllNodes(event: any): void {
        this.settings.pinAllNodes = event.target.checked;
        this.nodes.forEach((n: any) => n.fixed = this.settings.pinAllNodes);
    }

    handleChangeHideLabels(event: any): void {
        this.settings.hideAllLabels = event.target.checked;
        this.text.style("display", this.settings.hideAllLabels ? "none" : "inline");
    }

    handleCheckPinNode(event: any): void {
        this.selectedNode.fixed = event.target.checked;
    }

    handleClickCenterNode(_event: any): void {
        this.centerNodeInGraph(this.selectedNode);
    }

    renderFieldTemplate(label: String, value: String): String {
        return `<div class="field-row">
                    <span class="field">${label}</span>
                    <span>${value !== undefined ? value : "N/A"}</span>
                </div>`;
    }

    centerNodeInGraph(d: d3.layout.force.Node): void {
        if (d3.event) {
            d3.event.stopPropagation();
        }
        var dcx = (window.innerWidth / 2 - d.x * this.zoom.scale());
        var dcy = (window.innerHeight / 2 - d.y * this.zoom.scale());
        this.zoom.translate([dcx, dcy]);
        this.g.attr("transform", `translate(${dcx},${dcy}) scale(${this.zoom.scale()})`);
    }

    clearView(): void {
        this.svg.selectAll("*").remove();
    }

    exitHighlight(): void {
        this.nodeSummaryBox.style.display = "none";
        this.highlightNode = null;
        if (this.focusNode === null) {
            if (this.highlightColor != this.nodeFocusColor) {
                this.circle.style("fill", this.defaultNodeColor);
                this.text.style("font-weight", "normal");
                this.link.style("stroke", this.defaultLinkColor);
            }
        }
    }

    isConnected(a: any, b: any): Boolean {
        return this.linkedByIndex[a.id + "," + b.id] || this.linkedByIndex[b.id + "," + a.id] || a.id == b.id;
    }

    setFocus(d: any): void {
        if (this.highlightTrans < 1) {
            this.circle.style("opacity", (o: any) => this.isConnected(d, o) ? 1 : this.highlightTrans);
            this.text.style("opacity", (o: any) => this.isConnected(d, o) ? 1 : this.highlightTrans);
            this.link.style("opacity", (o: any) => o.source.id == d.id || o.target.id == d.id ? 1 : this.highlightTrans);
        }
    }

    setHighlight(d: any): void {
        if (this.focusNode !== null) {
            d = this.focusNode;
        }
        this.highlightNode = d;

        if (this.highlightColor != this.nodeFocusColor) {
            this.circle.style("fill", (o: any) => this.isConnected(d, o) ? this.highlightColor : this.nodeFocusColor);
            this.text.style("font-weight", (o: any) => this.isConnected(d, o) ? "bold" : "normal");
            this.link.style("stroke", (o: any) => o.source.id == d.id || o.target.id == d.id ? this.highlightColor : this.defaultLinkColor);
        }
    }

    resize(): void {
        var width = window.innerWidth, height = window.innerHeight;
        this.svg.attr("width", width).attr("height", height);
        this.force.size([
            this.force.size()[0] + (width - this.w) / this.zoom.scale(),
            this.force.size()[1] + (height - this.h) / this.zoom.scale(),
        ]).resume();
        this.w = width;
        this.h = height;
    }

    /**
     * Düğümlere tıklanınca sağ tarafda düğüm hakkında bilgi gösterir
     * @param node tıklanan düğüm
     */
    showNodeSummary(node: any): void {
        var nodeFieldsElement = document.getElementById('node-fields');
        this.elementRightSidePanel.style.display = "block";

        let checkPingNode = <HTMLInputElement>document.getElementById('check-pin-node');
        checkPingNode.checked = this.selectedNode.fixed;

        nodeFieldsElement.innerHTML = "";
        this.nodeInfoFields.forEach((field: any) => nodeFieldsElement.innerHTML += this.renderFieldTemplate(field.label, node.data[field.key]));
    }

    /**
     * Düğümler arası linklere tıklanınca sağ tarafda link hakkında bilgi gösterir
     * @param link tıklanan link
     */
    showLinkSummary(link: any): void {
        var linkFieldsElement = document.getElementById('link-fields');
        this.elementRightSideLinkPanel.style.display = "block";
        linkFieldsElement.innerHTML = "";
        this.linkInfoFields.forEach((field: any) => linkFieldsElement.innerHTML += this.renderFieldTemplate(field.label, link.data[field.key]));
    }

    public renderView(): void {
        this.clearView();

        const nodeById = d3.map();

        this.nodes.forEach((node: any) => nodeById.set(node.id, node));

        this.links.forEach(link => {
            link.source = nodeById.get(link.source.id ? link.source.id : link.source);
            link.target = nodeById.get(link.target.id ? link.target.id : link.target);

            this.linkedByIndex[link.source.id + "," + link.target.id] = true;
        });
        
        this.rect = this.svg.append("rect")
            .attr("width", "100%")
            .attr("height", "100%")
            .attr("fill", "#ecf0f1");

        this.g = this.svg.append("g");

        this.force
            .nodes(this.nodes)
            .links(this.links)
            .start();

        this.link = this.g.selectAll(".link")
            .data(this.links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", this.nominalStroke)
            .style("stroke", this.defaultLinkColor);

        this.node = this.g.selectAll(".node")
            .data(this.nodes)
            .enter().append("g")
            .attr("class", "node")
            .style("fill", this.defaultNodeColor)
            .call(this.force.drag);

        this.elementTotalNodeCount.innerText = this.nodes.length.toString();
        this.elementTotalLinkCount.innerText = this.links.length.toString();

        this.node.on("click", (d: any) => this.handleClickNode(d));

        this.link.on("click", (l: any) => this.handleClickLink(l));

        this.rect.on("click", () => this.handleClickGraphBackground());

        this.circle = this.node.append("path")
            .attr("d", d3.svg.symbol()
                .size(Math.PI * Math.pow(this.nominalBaseNodeSize, 2))
                .type(this.nodeShape)
            )
            .style("stroke", this.nodeFocusColor);

        this.text = this.g.selectAll(".text")
            .data(this.nodes)
            .enter().append("text")
            .attr("dy", ".35em")
            .style("fill", "#666")
            .style("font-size", this.nominalTextSize + "px")
            .text((d: any) => d.id);

        if (this.textCenter) {
            this.text.style("text-anchor", "middle");
        } else {
            this.text.attr("dx", this.nominalBaseNodeSize);
        }

        this.text.style("display", this.settings.hideAllLabels ? "none" : "inline");

        this.node
            .on("mouseover", (d: any) => this.setHighlight(d))
            .on("mousedown", (d: any) => this.onMouseDownNode(d))
            .on("mouseout", (_d: any) => this.exitHighlight());

        this.force.on("tick", () => this.forceOnTick());
    }

    onMouseDownNode(d: any): void {
        d3.event.stopPropagation();
        this.focusNode = d;
        this.setFocus(d);
        if (this.highlightNode === null) {
            this.setHighlight(d);
        }
    }

    forceOnTick(): void {
        this.node.attr("transform", (d: any) => `translate(${d.x},${d.y})`);
        this.text.attr("transform", (d: any) => `translate(${d.x},${d.y})`);

        this.link
            .attr("x1", (d: any) => d.source.x)
            .attr("y1", (d: any) => d.source.y)
            .attr("x2", (d: any) => d.target.x)
            .attr("y2", (d: any) => d.target.y);

        this.node
            .attr("cx", (d: any) => d.x)
            .attr("cy", (d: any) => d.y);

        this.text.attr("dx", "15px");
    }

    /**
     * Arkaplana tıklayınca çalışır. Düğüm seçildiğinde çıkan sağ tarafdaki panelleri gizlemek için ekrana tıklanabilir
     */
    handleClickGraphBackground(): void {
        if (!d3.event.defaultPrevented) {
            this.selectedNode = null;
            this.elementRightSidePanel.style.display = "none";
            this.elementRightSideLinkPanel.style.display = "none";
            d3.event.stopPropagation();
        }
    }

    /**
     * Düğüm ilişkilerine tıklayınca
     * @param l tıklanan link
     */
    handleClickLink(l: any): void {
        if (!d3.event.defaultPrevented) { // not dragged
            this.selectedLink = l;
            this.showLinkSummary(this.selectedLink);
        }
    }

    /**
     * Düğüme tıklayınca
     * @param d tıklanan düğüm
     */
    handleClickNode(d: any): void {
        if (!d3.event.defaultPrevented) { // not dragged
            this.selectedNode = d;
            this.showNodeSummary(this.selectedNode);

            // tıklanan düğümün alt düğümlerinin linkleri
            const nodeLinks = linkList.filter((l: any) => l.source == d.id);

            // bulunan linkler üzerinden graphda olmayan düğümler alınır.
            const targetsOfNode = nodeList.filter((n: any) =>
                nodeLinks.some((t: any) => t.target == n.id) && 
                !this.nodes.some((_n: any) => _n.id == n.id)
            );

            this.elementNodeRelationCount.innerText = nodeLinks.length.toString();

            this.nodes.push(...targetsOfNode);
            this.links.push(...nodeLinks);

            this.renderView();
        }
    }
}

// let forceDirectedGraph = new ForceDirectedGraph(nodeList, linkList);
let forceDirectedGraph = new ForceDirectedGraph(nodeList.slice(63,64), []);
forceDirectedGraph.renderView();
